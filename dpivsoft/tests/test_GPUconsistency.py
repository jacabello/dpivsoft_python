import unittest

# Standar libraries
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
import cv2
import time
import os

# DPIVSoft libraries
import dpivsoft.DPIV as DPIV      #Python PIV implementation
import dpivsoft.Cl_DPIV as Cl_DPIV   #OpenCL PIV implementation
import dpivsoft.SyIm as SyIm  #Syntetic images generator

from dpivsoft.Classes  import Parameters
from dpivsoft.Classes  import grid
from dpivsoft.Classes  import GPU
from dpivsoft.Classes  import Synt_Img

class TestDPIV(unittest.TestCase):
    def setUp(self):

        dirCode = os.getcwd()  # Current path
        dirImg = os.path.join(dirCode, "..", "Examples", "Images", "simple_tutorial")  # Join paths
        dirImg = os.path.abspath(dirImg)  # Convert to absolute path 

        if not os.path.exists(dirImg):
            os.makedirs(dirImg)
            SyIm.Analytic_Syntetic(dirImg, "Test_Img")

        # Setup: Load images and initialize OpenCL functions
        self.dirImg = dirImg #"../Examples/Images/simple_tutorial"
        self.files = ["Test_Img_1.png", "Test_Img_2.png"]

        self.name_img_1 = self.dirImg + '/' + self.files[0]
        self.name_img_2 = self.dirImg + '/' + self.files[1]

        Parameters.readParameters('../Examples/simple_tutorial_parameters.yaml')

        # Load images
        self.Img1, self.Img2 = DPIV.load_images(self.name_img_1, self.name_img_2)
        self.height, self.width = self.Img1.shape

        # Initialize OpenCL functions
        self.thr = Cl_DPIV.select_Platform(0)
        os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'

        # Send first pair of images to GPU
        GPU.img1 = self.thr.to_device(self.Img1)
        GPU.img2 = self.thr.to_device(self.Img2)

        # Compile kernels and initialize variables
        Cl_DPIV.compile_Kernels(self.thr)

        # Generate x-y mesh
        grid.generate_mesh(self.Img1.shape[1], self.Img1.shape[0])
        Cl_DPIV.initialization(self.width, self.height, self.thr)

        # Pixels used
        self.N_boxes_1 = Parameters.no_boxes_1_x * Parameters.no_boxes_1_y
        self.N_pixels_1 = self.N_boxes_1 * Parameters.box_size_1_x * Parameters.box_size_1_y
        self.N_boxes_2 = Parameters.no_boxes_2_x*Parameters.no_boxes_2_y
        self.pixels = Parameters.box_size_1_y * Parameters.box_size_1_x

        #Get the full subImg1 using CPU method
        cont = 0
        GPU_subImg1 = np.real(GPU.subImg1_1.get())
        self.SubImg1 = np.zeros(GPU_subImg1.shape)
        self.SubImg2 = np.zeros(GPU_subImg1.shape)
        self.SubImg1_norm = np.zeros(GPU_subImg1.shape)
        self.SubImg2_norm = np.zeros(GPU_subImg1.shape)
        self.Correlation = np.zeros(GPU_subImg1.shape)
        self.u1 = np.zeros([Parameters.no_boxes_1_y, Parameters.no_boxes_1_x])
        self.v1 = np.zeros([Parameters.no_boxes_1_y, Parameters.no_boxes_1_x])

        [Height,Width] = self.Img1.shape
        i_matrix, j_matrix = np.meshgrid(
                np.arange(0, Parameters.box_size_1_x),
                np.arange(0, Parameters.box_size_1_y))

        # Define origin of boxes without translation
        self.u_index = np.zeros(self.SubImg1.shape)
        self.v_index = np.zeros(self.SubImg1.shape)
        self.i_index = np.zeros(self.SubImg1.shape)
        self.j_index = np.zeros(self.SubImg1.shape)
        self.i_frac_1 = np.zeros(self.SubImg1.shape)
        self.i_frac_2 = np.zeros(self.SubImg1.shape)
        self.j_frac_1 = np.zeros(self.SubImg1.shape)
        self.j_frac_2 = np.zeros(self.SubImg1.shape)
        self.cpu_subImg1 = np.zeros(self.SubImg1.shape)
        self.cpu_subImg2 = np.zeros(self.SubImg1.shape)

        for j in range(0, Parameters.no_boxes_1_y):
            for i in range(0, Parameters.no_boxes_1_x):
                box_o_y = int(grid.box_origin_y_1[j, i])
                box_o_x = int(grid.box_origin_x_1[j, i])

                self.SubImg1[cont, :, :] = (
                    self.Img1[box_o_y: box_o_y + Parameters.box_size_1_y,
                              box_o_x: box_o_x + Parameters.box_size_1_x])
                self.SubImg2[cont, :, :] = (
                    self.Img2[box_o_y: box_o_y + Parameters.box_size_1_y,
                              box_o_x: box_o_x + Parameters.box_size_1_x])

                self.SubImg1_norm[cont, :, :] = self.SubImg1[cont,:,:] - np.sum(self.SubImg1[cont,:,:]) / self.pixels
                self.SubImg2_norm[cont, :, :] = self.SubImg2[cont,:,:] - np.sum(self.SubImg2[cont,:,:]) / self.pixels


                # Cross Correlation Function of the pair images
                self.Correlation[cont,:,:] = (np.fft.fftshift(np.real(np.fft.ifft2(
                    np.multiply(np.conj(np.fft.fft2(self.SubImg1_norm[cont,:,:])),
                    np.fft.fft2(self.SubImg2_norm[cont,:,:])))))/(1))

                epsilon_x, epsilon_y, col_idx, row_idx = DPIV.find_peaks(
                    self.Correlation[cont,:,:], Parameters.window_1_x, Parameters.window_1_y)

                self.u1[j,i] = epsilon_x + col_idx-Parameters.box_size_1_x/2
                self.v1[j,i] = epsilon_y + row_idx-Parameters.box_size_1_y/2

                cont += 1

        # Obtain derivatives
        self.du_dx, self.du_dy, self.dv_dx, self.dv_dy = DPIV.jacobian_matrix(
            self.u1, self.v1, grid.x_1, grid.y_1, Parameters.no_boxes_1_x, Parameters.no_boxes_1_y)

        cont = 0
        for j in range(0, Parameters.no_boxes_1_y):
            for i in range(0, Parameters.no_boxes_1_x):
                # Obtain deformed image.
                SubImg1, SubImg2, u_index, v_index = DPIV.deform_image(
                        self.Img1, self.Img2, Width, Height, grid.box_origin_x_1, grid.box_origin_y_1,
                        i_matrix, j_matrix, Parameters.box_size_1_x, 
                        Parameters.box_size_1_y, self.u1, self.v1, self.du_dx, 
                        self.du_dy, self.dv_dx, self.dv_dy, i, j)
        
                self.cpu_subImg1[cont,:,:] = SubImg1
                self.cpu_subImg2[cont,:,:] = SubImg2
                self.u_index[cont,:,:] = u_index
                self.v_index[cont,:,:] = v_index

                cont += 1

    def test_grid_points(self):
        # Test grid points between CPU and GPU
        cpu_x1 = grid.box_origin_x_1
        gpu_x1 = GPU.box_origin_x_1.get()
        cpu_y1 = grid.box_origin_y_1
        gpu_y1 = GPU.box_origin_y_1.get()
        cpu_x2 = grid.box_origin_x_2
        gpu_x2 = GPU.box_origin_x_2.get()
        cpu_y2 = grid.box_origin_y_2
        gpu_y2 = GPU.box_origin_y_2.get()

        np.testing.assert_array_equal(gpu_x1, cpu_x1, "box_origin_x_1 is not the same")
        np.testing.assert_array_equal(gpu_y1, cpu_y1, "box_origin_y_1 is not the same")
        np.testing.assert_array_equal(gpu_x2, cpu_x2, "box_origin_x_2 is not the same")
        np.testing.assert_array_equal(gpu_y2, cpu_y2, "box_origin_y_2 is not the same")

    def test_sub_images(self):
        # Test subImage slices between CPU and GPU

        GPU.Slice(GPU.subImg1_1, GPU.img1, GPU.box_origin_x_1, GPU.box_origin_y_1,
                  GPU.data1, local_size=None, global_size=self.N_pixels_1)
        GPU_subImg1 = np.real(GPU.subImg1_1.get())

        np.testing.assert_array_equal(GPU_subImg1, self.SubImg1, "SubImages are not the same")

    def test_normalization(self):
        # Test normalization between CPU and GPU
        complex_SubImg1 = np.array(self.SubImg1, dtype=np.complex64)  # Adds zero imaginary part
        GPU.subImg1_1 = self.thr.to_device(complex_SubImg1)

        GPU.SubMean(GPU.subMean1_1, GPU.subImg1_1, GPU.data1,
                    local_size=None, global_size=self.N_boxes_1)
        GPU.Normalize(GPU.subImg1_1, GPU.subMean1_1, GPU.data1,
                      local_size=None, global_size=self.N_pixels_1)
        GPU_subImg1_norm = np.real(GPU.subImg1_1.get())

        np.testing.assert_allclose(
            GPU_subImg1_norm, self.SubImg1_norm, 
            err_msg="SubImages normalization is not the same")

    def test_crossCorrelation(self):

        # Send values to GPU
        complex_SubImg1 = np.array(self.SubImg1_norm, dtype=np.complex64)
        GPU.subImg1_1 = self.thr.to_device(complex_SubImg1)
        complex_SubImg2 = np.array(self.SubImg2_norm, dtype=np.complex64)
        GPU.subImg1_2 = self.thr.to_device(complex_SubImg2)

        # FFT2D
        GPU.fft(GPU.subImg1_1, GPU.subImg1_1)
        GPU.fft(GPU.subImg1_2, GPU.subImg1_2)
        # Conjugate
        GPU.subImg1_1 = GPU.subImg1_1.conj()
        # Multiplication
        GPU.multiply_them(GPU.subImg1_1, GPU.subImg1_1, GPU.subImg1_2,
                local_size = None, global_size = self.N_pixels_1)
        # Inverse transform
        GPU.fft(GPU.subImg1_1, GPU.subImg1_1, inverse=True)
        # FFTShift
        GPU.fftshift(GPU.subImg1_1, GPU.subImg1_1)
        GPU_correlation = np.real(GPU.subImg1_1.get())

        np.testing.assert_allclose(
            GPU_correlation, self.Correlation, 
            rtol=1e-3, atol=1, err_msg="Correlations are not the same")

    def test_findPeak(self):

        complex_SubImg1 = np.array(self.Correlation, dtype=np.complex64)
        GPU.subImg1_1 = self.thr.to_device(complex_SubImg1)

        GPU.ini_index(GPU.u_index_1, GPU.v_index_1, local_size = None,
                global_size = self.N_pixels_1)
        GPU.find_peak(GPU.v1, GPU.u1, GPU.subImg1_1, GPU.u_index_1,
                GPU.v_index_1, GPU.data1, local_size = None,
                global_size = self.N_boxes_1)

        g_v1 = GPU.v1.get()
        g_u1 = GPU.u1.get()

        np.testing.assert_allclose(
            g_u1, self.u1, rtol=1e-6, atol=1e-1, 
            err_msg="X-velocity is not the same")
        np.testing.assert_allclose(
            g_v1, self.v1, rtol=1e-6, atol=1e-1, 
            err_msg="Y-velocity is not the same")

    def test_derivatives(self):

        rtol = 1e-5
        atol = 1e-3

        GPU.u1_f = self.thr.to_device(np.float32(self.u1))
        GPU.v1_f = self.thr.to_device(np.float32(self.v1))

        GPU.jacobian(GPU.temp_dx_1, GPU.temp_dy_1, GPU.u1_f, GPU.x1, GPU.y1,
                GPU.data1, local_size = None, global_size = self.N_boxes_1)
        GPU.box_blur(GPU.du_dx_1, GPU.temp_dx_1, GPU.data1,
                local_size = None, global_size = self.N_boxes_1)
        GPU.box_blur(GPU.du_dy_1, GPU.temp_dy_1, GPU.data1,
                local_size = None, global_size = self.N_boxes_1)

        GPU.jacobian(GPU.temp_dx_1, GPU.temp_dy_1, GPU.v1_f, GPU.x1, GPU.y1,
                GPU.data1, local_size = None, global_size = self.N_boxes_1)
        GPU.box_blur(GPU.dv_dx_1, GPU.temp_dx_1, GPU.data1,
                local_size = None, global_size = self.N_boxes_1)
        GPU.box_blur(GPU.dv_dy_1, GPU.temp_dy_1, GPU.data1,
                local_size = None, global_size = self.N_boxes_1)

        g_du_dx = GPU.du_dx_1.get()
        g_du_dy = GPU.du_dy_1.get()
        g_dv_dx = GPU.dv_dx_1.get()
        g_dv_dy = GPU.dv_dy_1.get()

        np.testing.assert_allclose(
            self.du_dx, g_du_dx, rtol=rtol, atol=atol, err_msg="du/dx is not the same")
        np.testing.assert_allclose(
            self.du_dy, g_du_dy, rtol=rtol, atol=atol, err_msg="dv/dy is not the same")
        np.testing.assert_allclose(
            self.dv_dx, g_dv_dx, rtol=rtol, atol=atol, err_msg="dv/dx is not the same")
        np.testing.assert_allclose(
            self.dv_dy, g_dv_dy, rtol=rtol, atol=atol, err_msg="dv/dy is not the same")

    def test_interpolation(self):
        # Check validity of bi-linear interpolation (not the same used on cpu version).

        GPU.u1 = self.thr.to_device(np.float32(self.u1))
        GPU.v1 = self.thr.to_device(np.float32(self.v1))
        GPU.interpolate(GPU.u2_f, GPU.u1, GPU.x2, GPU.y2, GPU.x1, GPU.y1,
                GPU.data1, local_size = None, global_size = self.N_boxes_2)
        GPU.interpolate(GPU.v2_f, GPU.v1, GPU.x2, GPU.y2, GPU.x1, GPU.y1,
                GPU.data1, local_size = None, global_size = self.N_boxes_2)
        g_u2 = GPU.u2_f.get()
        g_v2 = GPU.v2_f.get()

        # This function interpolate the first grid into the second
        no_box = Parameters.no_boxes_1_y * Parameters.no_boxes_1_x

        interp_func = interpolate.RegularGridInterpolator(
            (grid.y_1[:,0], grid.x_1[0,:]), self.u1, method='linear')
        points = np.array([grid.y_2.ravel(), grid.x_2.ravel()]).T
        u2 = interp_func(points).reshape(grid.x_2.shape)

        interp_func = interpolate.RegularGridInterpolator(
            (grid.y_1[:,0], grid.x_1[0,:]), self.v1, method='linear')
        points = np.array([grid.y_2.ravel(), grid.x_2.ravel()]).T
        v2 = interp_func(points).reshape(grid.x_2.shape)

        np.testing.assert_allclose(g_u2, u2, rtol=1e-1, atol=1e-2,
                    err_msg="Error in X-velocity interpolation")
        np.testing.assert_allclose(g_v2, v2, rtol=1e-1, atol=1e-2,
                    err_msg="Error in Y-velocity interpolation")

    def test_deformation(self):

        # Check deformation
        #============================================================================
        GPU.u1 = self.thr.to_device(np.float32(self.u1))
        GPU.v1 = self.thr.to_device(np.float32(self.v1))
        GPU.du_dx_1 = self.thr.to_device(np.float32(self.du_dx))
        GPU.du_dy_1 = self.thr.to_device(np.float32(self.du_dy))
        GPU.dv_dx_1 = self.thr.to_device(np.float32(self.dv_dx))
        GPU.dv_dy_1 = self.thr.to_device(np.float32(self.dv_dy))

        GPU.ini_index(GPU.u_index_1, GPU.v_index_1, local_size = None,
                global_size = self.N_pixels_1)

        # Deformed image
        GPU.deform_image(GPU.subImg1_1, GPU.subImg1_2, GPU.img1, GPU.img2,
                GPU.box_origin_x_1, GPU.box_origin_y_1, GPU.u1,
                GPU.v1, GPU.du_dx_1, GPU.du_dy_1, GPU.dv_dx_1,
                GPU.dv_dy_1, GPU.u_index_1, GPU.v_index_1, GPU.data1,
                local_size = None, global_size = self.N_pixels_1)

        # Normalize image to compare (GPU cannot do the normalization, 
        # it should not have significant impact in final result)
        gpu_subImg1 = np.real(GPU.subImg1_1.get())
        slice_means = gpu_subImg1.mean(axis=(1, 2), keepdims=True)
        gpu_subImg1 = gpu_subImg1 - slice_means

        # Compare only inside windows as long as image boundarys can 
        # only be deformed by individual pixels 
        n = np.arange(0,len(gpu_subImg1[:,1,1]))
        rows = n // Parameters.no_boxes_1_x
        cols = n % Parameters.no_boxes_1_x
        mask = ((rows > 0) & (rows < Parameters.no_boxes_1_y-1) & (cols > 0) 
                & (cols < Parameters.no_boxes_1_x-1))
        fn = n[mask]

        np.testing.assert_allclose(
            gpu_subImg1[fn,:,:], self.cpu_subImg1[fn,:,:], rtol=1e-3, atol=1e-2,
            err_msg="Difference in image deformation")

if __name__ == '__main__':
    unittest.main()